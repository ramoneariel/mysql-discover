<h1> * MYSQL DISCOVER * </h1>
<h3> Documentation guide </h3>


First you need to make some settings to use 
the package:

<code>

const express = require('express')
const discover = require('mysql-discover')
const database_config = {
    host : 'your_host',
    user : 'your_user',
    password : 'your_strong_password',
    database : 'your_database'
}

const app = express();

discover.run(app,database_config);

app.listen(your_port,()=>{
    console.log('Works!');
})

</code>


Then, in your console, you can see de models and
entry points discovered on database pased.


<footer>
    <small> Thanks for use it, <span> &#169;</span> Mascareño, Ricardo Matias. </small>
</footer>