const _console = require('Console');
const generate_entry_points = require('./entry_points');

async function discover(pool,db){
    _console.warn('Looking for models...');
    let tables = await get_all_tables(pool,db);
    let relations = await get_relations(tables)
    let models = await get_models(tables)
    await model_relations(models,relations);
    await generate_entry_points(pool,models,relations)
    
}


async function get_all_tables(pool,db){
    let tables = [];
    let tableQuery = await pool.query(`SHOW TABLES`)
    tableQuery.forEach(table => {
        tables.push(table["Tables_in_"+db]);
    });
    return tables;
}


async function get_models(tables){
    let models = [];
    await tables.forEach((table) => {
        let res = table.split('_');
        if(res[0] === table){
            models.push(table)    
        }
    });
    return models;
}


async function get_relations(tables){
    let relations = []
    await tables.forEach((table) => {
        let res = table.split('_');
        if(res[0] !== table){
            relations.push(table)    
        }
    });
    return relations;
}

async function model_relations(models,relations){
    models.forEach(model => {
        _console.success(`✔ Model ${model} Discovered...`);
        let model_singular = model.substr(0,model.length-1);
        relations.forEach(relation => {
            let rel = relation.split('_');
            if(rel[0] == model_singular ){
                _console.stress(`  ➣  ${model} Has Depedency With ${rel[1]}`)
            }
            if(rel[1] == model){
                _console.error(`  ➣  ${model} Has External Relationship With ${rel[0]}`)
            }
        });
    })
}



module.exports = discover;
