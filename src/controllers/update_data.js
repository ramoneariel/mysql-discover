async function update_data(pool,model,id,data){
    let query = await pool.query(`UPDATE ${model} SET ? WHERE id = ?`,[data,id]);
    return query;
}

module.exports = update_data;