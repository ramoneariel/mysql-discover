async function store_data(pool,model,data){
    let query = await pool.query(`INSERT INTO ${model} SET ?`,[data])
    return query;
}

module.exports = store_data;